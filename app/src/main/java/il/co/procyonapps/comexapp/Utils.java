package il.co.procyonapps.comexapp;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import il.co.procyonapps.comexapp.objects.Companies;
import il.co.procyonapps.comexapp.objects.Company;

/**
 * Created by Hanan on 21/12/2017.
 */

public class Utils {


    public static List<Company> getCompaniesFromResource() {
        try {
            InputStream inputStream = ComaxApp.mComaxApp.get().getAssets().open("companies.json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();

            String companies = new String(buffer, "UTF-8");
            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

            final Companies companies1 = gson.fromJson(companies, Companies.class);
            Log.d("Utils", "getCompaniesFromResource: " + companies1.companies);
            return companies1.companies;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
