package il.co.procyonapps.comexapp.viewmodels;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import il.co.procyonapps.comexapp.data.DbManager;
import il.co.procyonapps.comexapp.objects.Company;

/**
 * Created by Hanan on 24/12/2017.
 */

public class DetailsViewModel extends ViewModel {

    private LiveData<Company> mCompanyLiveData;
    private int currentCompany;

    public LiveData<Company> getCompanyLiveData(int companyId){
        if (mCompanyLiveData == null || currentCompany != companyId) {
            mCompanyLiveData = DbManager.getInstance().getCompany(companyId);
            currentCompany = companyId;

        }

        return mCompanyLiveData;
    }
}
