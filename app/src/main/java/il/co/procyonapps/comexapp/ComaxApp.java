package il.co.procyonapps.comexapp;

import android.app.Application;

import java.lang.ref.WeakReference;

/**
 * Created by Hanan on 23/12/2017.
 */

public class ComaxApp extends Application {

    public static WeakReference<ComaxApp> mComaxApp;

    @Override
    public void onCreate() {
        super.onCreate();

        mComaxApp =new WeakReference<ComaxApp>(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        mComaxApp = null;
    }
}
