package il.co.procyonapps.comexapp.network;

import il.co.procyonapps.comexapp.objects.GSearchResults;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Hanan on 23/12/2017.
 */

public interface GoogleSearchAPI {

    @GET("/customsearch/v1?cr=IL&cx=016568986584447868253%3Apfet35gaf9a&num=1&searchType=image&key=AIzaSyCpIBZzFCFPwN2E64BwPvcg7XGefhGHFJU")
    Call<GSearchResults> getSearchResults(@Query("q") String company);
}
