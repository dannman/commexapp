package il.co.procyonapps.comexapp.data;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;

import il.co.procyonapps.comexapp.ComaxApp;
import il.co.procyonapps.comexapp.Utils;
import il.co.procyonapps.comexapp.objects.Company;

/**
 * Created by Hanan on 21/12/2017.
 */

public class DbManager {

    private final String TAG = this.getClass().getSimpleName();
    private static DbManager ourInstance;

    public static DbManager getInstance() {
        if (ourInstance == null) {
            ourInstance = new DbManager();
        }
        return ourInstance;
    }

    private CompanyDatabase mCompanyDatabase;

    private DbManager() {

        mCompanyDatabase = Room.databaseBuilder(ComaxApp.mComaxApp.get(), CompanyDatabase.class, "company-db").addCallback(mCallback).build();
    }

    public CompanyDatabase getCompanyDatabase() {
        return mCompanyDatabase;
    }

    RoomDatabase.Callback mCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            //add all initial data from file to Room
            MyExecutors.getInstance().getBackgroundExecutor().execute(() -> {
                List<Company> companies = Utils.getCompaniesFromResource(/*mWeakContext.get()*/);
                long[] inserts = mCompanyDatabase.daoCompany().insertCompanyGroup(companies);
                Log.d(TAG, "onCreateDB: inserted= " + inserts);
            });

        }

        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
        }
    };


    public LiveData<List<Company>> getAllCompanies(){
        return mCompanyDatabase.daoCompany().getAllCompanies();
    }

    public LiveData<List<Company>> getCompaniesBatch(int startingIndex){
        return mCompanyDatabase.daoCompany().getCompaniesInRange(startingIndex);
    }

    public LiveData<List<Company>> getCompaniesUpTo(int upTo){
        return mCompanyDatabase.daoCompany().getCompaniesUpTo(upTo);
    }

    public void updateCompany(Company company){
        mCompanyDatabase.daoCompany().updateCompany(company);
    }

    public LiveData<Company> getCompany(int id){
        return mCompanyDatabase.daoCompany().getCompanyDetails(id);
    }

    public void addCompany(Company company){
        MyExecutors.getInstance().getBackgroundExecutor().execute(() -> mCompanyDatabase.daoCompany().insertCompany(company));

    }

    public void deleteAll(){
        MyExecutors.getInstance().getBackgroundExecutor().execute(() -> mCompanyDatabase.daoCompany().deleteAll());
    }

    public void deleteCompanyWithDelay(Company company){
        MyExecutors.getInstance().getBackgroundExecutor().execute(() ->{
                mCompanyDatabase.daoCompany().deleteCompany(company);
        });
    }


}
