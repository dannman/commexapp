package il.co.procyonapps.comexapp.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.ViewModel;
import android.text.TextUtils;
import android.util.Log;

import java.io.IOException;
import java.util.List;

import il.co.procyonapps.comexapp.data.DbManager;
import il.co.procyonapps.comexapp.data.MyExecutors;
import il.co.procyonapps.comexapp.network.ApiManager;
import il.co.procyonapps.comexapp.objects.Company;
import il.co.procyonapps.comexapp.objects.GSearchResults;
import il.co.procyonapps.comexapp.objects.Item;
import retrofit2.Response;

/**
 * Created by Hanan on 23/12/2017.
 */

public class MainViewModel extends ViewModel {

    private final String TAG = this.getClass().getSimpleName();
    private MediatorLiveData<List<Company>> mCompanies;
    private DbManager mDbManager;
    private int mListSize;

    public MediatorLiveData<List<Company>> getCompanies() {
        if (mCompanies == null) {
            mCompanies = new MediatorLiveData<>();
        }


        return mCompanies;
    }

    public void initializeImageFill(){
        //initialize stuff:
        LiveData<List<Company>> liveData = DbManager.getInstance().getAllCompanies();
        mListSize = 0;
        liveData.observeForever(companies -> {
            if(companies != null && !companies.isEmpty() && mListSize != companies.size()){
                mListSize = companies.size();
                populateImages(companies);
            }
        });
    }

    public void loadCompanies(int fromIndex) {
        Log.d(TAG, "loadCompanies: from index=" + fromIndex);
        mDbManager = DbManager.getInstance();
        getCompanies().addSource(mDbManager.getCompaniesUpTo(fromIndex + 5), companies -> {
                    Log.d(TAG, "loadCompanies: size= " + companies.size() + "\n" + companies.toString());
                    getCompanies().setValue(companies);

                }
        );
    }

    private void populateImages(List<Company> companies) {
        MyExecutors.getInstance().getNetworkExecutor().execute(() -> {
                    for (Company company : companies) {
                        if (TextUtils.isEmpty(company.getLogoThumbUrl()) || company.getLogoThumbUrl().equals("null")) {
                            try {
                                Response<GSearchResults> response = ApiManager.getInstance().services().getSearchResults(company.getCompanyName() + " logo").execute();

                                if (response.isSuccessful()) {

                                    Item item = response.body().getItems().get(0);
                                    String newUrl = null;

                                    if (!TextUtils.isEmpty(item.getLink())) {
                                        newUrl = item.getLink();
                                    } else if (!TextUtils.isEmpty(item.getImage().getThumbnailLink())) {
                                        newUrl = item.getImage().getThumbnailLink();

                                    }
                                    if (newUrl != null) {
                                        Log.d(TAG, "populateImages: Company= " + company.getCompanyName() + ", newUrl= " + newUrl);
                                        company.setLogoThumbUrl(newUrl);
                                        mDbManager.updateCompany(company);
                                    }
                                } else {
                                    Log.d(TAG, "populateImages: error= " + response.errorBody().string());
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
        );
    }
}
