package il.co.procyonapps.comexapp.views;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import il.co.procyonapps.comexapp.R;
import il.co.procyonapps.comexapp.objects.Company;
import il.co.procyonapps.comexapp.viewmodels.DetailsViewModel;


/**
 * Created by Hanan on 24/12/2017.
 */

public class CompanyDetailsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final String COMPANY_ID = "COMPANY_ID";
    private TextView mTitle;
    private TextView mAddress;
    private ImageView mCompanyLogo;
    private LatLng mLatLng;
    private SupportMapFragment mMapFragment;
    private GoogleMap mMap;

    public static Intent initActivity(Context context, int companyId) {
        Intent intent = new Intent(context, CompanyDetailsActivity.class);
        intent.putExtra(COMPANY_ID, companyId);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

//        getActionBar().setDisplayShowHomeEnabled(true);
//        getActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        DetailsViewModel viewModel = ViewModelProviders.of(this).get(DetailsViewModel.class);
        int companyId = getIntent().getIntExtra(COMPANY_ID, -1);
        final LiveData<Company> companyLiveData = viewModel.getCompanyLiveData(companyId);

        //find views
        mTitle = (TextView) findViewById(R.id.tv_company_name);
        mAddress = (TextView) findViewById(R.id.tv_address);
        mCompanyLogo = (ImageView) findViewById(R.id.iv_company_image);

        companyLiveData.observe(this, company -> {
            loadCompany(company);
            mLatLng = new LatLng(company.getLat(), company.getLng());
            if (mMap != null) {
                centerMap(mMap);
            }

        });

        mMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_frag);
        mMapFragment.getMapAsync(this);


    }

    private void loadCompany(Company company) {
        mTitle.setText(company.getCompanyName());
        mAddress.setText(getString(R.string.address, company.getCity(), company.getAddress()));
        Picasso.with(this).load(company.getLogoThumbUrl()).into(mCompanyLogo);
        setTitle(company.getCompanyName() + " - פרטים");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        centerMap(googleMap);
    }

    private void centerMap(GoogleMap map){
        if(mLatLng != null) {
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(mLatLng, 16f));
            map.addMarker(new MarkerOptions().position(mLatLng));
        }
    }
}
