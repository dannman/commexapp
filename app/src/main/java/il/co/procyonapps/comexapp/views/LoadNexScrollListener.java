package il.co.procyonapps.comexapp.views;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by Hanan on 23/12/2017.
 */

public abstract class LoadNexScrollListener extends RecyclerView.OnScrollListener {


    private LinearLayoutManager mLayoutManager;
    private boolean isLoading = true;
    private int previousTotal = 0;
    int firstVisibleItem, visibleItemCount, totalItemCount;


    public LoadNexScrollListener(LinearLayoutManager layoutManager) {
        mLayoutManager = layoutManager;


    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        visibleItemCount = recyclerView.getChildCount();
        totalItemCount = mLayoutManager.getItemCount();
        firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();

        if (isLoading){
            if (totalItemCount > previousTotal){
                isLoading = false;
                previousTotal = totalItemCount;
            }
        }

        if (!isLoading && (totalItemCount - visibleItemCount) <= firstVisibleItem){
            isLoading = true;
            loadNext(totalItemCount);
        }
    }


    abstract void loadNext(int lastItem);
}
