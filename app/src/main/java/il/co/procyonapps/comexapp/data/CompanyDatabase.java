package il.co.procyonapps.comexapp.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import il.co.procyonapps.comexapp.objects.Company;

/**
 * Created by Hanan on 21/12/2017.
 */

@Database(entities = {Company.class}, version = 1)
public abstract class CompanyDatabase extends RoomDatabase {
    public abstract DaoCompany daoCompany();
}
