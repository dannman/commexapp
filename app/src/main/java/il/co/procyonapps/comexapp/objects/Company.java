package il.co.procyonapps.comexapp.objects;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Hanan on 21/12/2017.
 */

@Entity
public class Company {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "company_name")
    @SerializedName("name")
    @Expose
    private String companyName;

    @ColumnInfo(name = "description")
    private String description;

    @ColumnInfo(name = "lat")
    @SerializedName("latitude")
    @Expose
    private double lat;

    @ColumnInfo(name = "lng")
    @SerializedName("longitude")
    @Expose
    private double lng;

    @ColumnInfo(name = "city")
    @SerializedName("city")
    @Expose
    private String city;

    @ColumnInfo(name = "address")
    @SerializedName("address")
    @Expose
    private String address;

    @ColumnInfo(name = "logoUrl")
    private String logoThumbUrl;

    public Company() {
    }

    public Company( String companyName, double lat, double lng, String city, String address, String imageUrl) {
//        this.id = id;
        this.companyName = companyName;
//        this.description = description;
        this.lat = lat;
        this.lng = lng;
        this.city = city;
        this.address = address;
        logoThumbUrl = imageUrl;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setLogoThumbUrl(String logoThumbUrl) {
        this.logoThumbUrl = logoThumbUrl;
    }

    public int getId() {
        return id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getDescription() {
        return description;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public String getLogoThumbUrl() {
        return logoThumbUrl;
    }

    @Override
    public String toString() {
        return "Company{" +
                "id=" + id +
                ", companyName='" + companyName + '\'' +
                ", description='" + description + '\'' +
                ", lat=" + lat +
                ", lng=" + lng +
                ", city='" + city + '\'' +
                ", address='" + address + '\'' +
                ", logoThumbUrl='" + logoThumbUrl + '\'' +
                '}';
    }
}
