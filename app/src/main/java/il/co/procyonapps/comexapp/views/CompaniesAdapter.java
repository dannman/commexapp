package il.co.procyonapps.comexapp.views;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import il.co.procyonapps.comexapp.R;
import il.co.procyonapps.comexapp.objects.Company;

/**
 * Created by Hanan on 23/12/2017.
 */

public class CompaniesAdapter extends RecyclerView.Adapter {

    List<Company> mCompanies;
    private ListClickListener mListener;

    public void setData(List<Company> companies) {
        if (mCompanies == null) {
            mCompanies = new ArrayList<>();
        } else {
            mCompanies.clear();
        }
        mCompanies = companies;
        notifyDataSetChanged();


    }

    public void removeItem(int position){
        notifyItemRemoved(position);
    }

    public void setOnItmeClickListener(ListClickListener listener){
        mListener = listener;
    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_company, parent, false);
        return new CompanyVH(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Company company = mCompanies.get(position);
        ((CompanyVH)holder).setCompany(company);

        holder.itemView.setOnClickListener(v -> {

            if (mListener != null) {
                mListener.onItemClick(mCompanies.get(position).getId());
            }
        });


    }

    @Override
    public int getItemCount() {
        return mCompanies == null ? 0 : mCompanies.size();
    }

    private class CompanyVH extends RecyclerView.ViewHolder {

        private final ImageView mLogo;
        private final TextView mComapnyName;
        private final TextView mComapnyAddress;

        public CompanyVH(View itemView) {
            super(itemView);
            mLogo = (ImageView) itemView.findViewById(R.id.iv_logo);
            mComapnyName = (TextView) itemView.findViewById(R.id.et_name);
            mComapnyAddress = (TextView) itemView.findViewById(R.id.tv_address);
        }

        public void setCompany(Company company) {
            Picasso.with(mLogo.getContext()).load(company.getLogoThumbUrl()).error(R.mipmap.ic_launcher).placeholder(R.mipmap.ic_launcher).into(mLogo);
            mComapnyName.setText(company.getCompanyName());
            mComapnyAddress.setText(itemView.getContext().getString(R.string.address, company.getCity(), company.getAddress()) );

        }
    }

    public interface ListClickListener{
        void onItemClick(int id);
    }
}
