package il.co.procyonapps.comexapp.data;

/**
 * Created by Hanan on 23/12/2017.
 */

public interface DBCallback<T> {

    void result(T result);
}
