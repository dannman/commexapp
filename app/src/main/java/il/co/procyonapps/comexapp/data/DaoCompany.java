package il.co.procyonapps.comexapp.data;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import il.co.procyonapps.comexapp.objects.Company;

/**
 * Created by Hanan on 21/12/2017.
 */

@Dao
public interface DaoCompany {

    // TODO: 21/12/2017 populate methods

    @Insert
    long insertCompany(Company company);

    @Insert
    long[] insertCompanyGroup(List<Company> companies);

    @Update
    void updateCompany(Company company);

    @Delete
    void deleteCompany(Company company);

    @Query("DELETE FROM Company WHERE id > 0")
    void deleteAll();

    @Query("SELECT * FROM Company")
    LiveData<List<Company>> getAllCompanies();

    @Query("SELECT * FROM Company WHERE id >= :start AND id <= (:start + 3)")
    LiveData<List<Company>> getCompaniesInRange(int start);

    @Query("SELECT * FROM Company WHERE id <= :limit")
    LiveData<List<Company>> getCompaniesUpTo(int limit);

    @Query("SELECT * FROM Company WHERE id == :id LIMIT 1")
    LiveData<Company> getCompanyDetails(int id);
}
