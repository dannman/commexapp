package il.co.procyonapps.comexapp.data;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

;

/**
 * Created by Hanan on 23/12/2017.
 */

public class MyExecutors {

    private Executor mUiExecutor;
    private Executor mBackgroundExecutor;
    private Executor mNetworkExecutor;

    private static MyExecutors mInstance;

    public static MyExecutors getInstance(){
        if (mInstance == null) {
            mInstance = new MyExecutors();
        }
        return mInstance;
    }



    private MyExecutors(){
        mUiExecutor = new UiExecutor();
        mBackgroundExecutor = Executors.newSingleThreadExecutor();
        mNetworkExecutor = Executors.newFixedThreadPool(3);

    }

    public Executor getUiExecutor() {
        return mUiExecutor;
    }

    public Executor getBackgroundExecutor() {
        return mBackgroundExecutor;
    }

    public Executor getNetworkExecutor() {
        return mNetworkExecutor;
    }

    private static class UiExecutor implements Executor {

        private final Handler mHandler = new Handler(Looper.getMainLooper());

        @Override
        public void execute(@NonNull Runnable command) {
            mHandler.post(command);
        }
    }
}
