package il.co.procyonapps.comexapp.views;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import il.co.procyonapps.comexapp.R;
import il.co.procyonapps.comexapp.data.DbManager;
import il.co.procyonapps.comexapp.objects.Company;
import il.co.procyonapps.comexapp.viewmodels.MainViewModel;

public class MainActivity extends AppCompatActivity {
    private final String TAG = this.getClass().getSimpleName();

    MainViewModel mMainViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView companiesRv = findViewById(R.id.rv_companies);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        CompaniesAdapter companiesAdapter = new CompaniesAdapter();

        companiesRv.setLayoutManager(layoutManager);
        companiesRv.setAdapter(companiesAdapter);

        mMainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        companiesRv.addOnScrollListener(new LoadNexScrollListener(layoutManager) {
            @Override
            void loadNext(int lastItem) {
                Toast.makeText(MainActivity.this, "last item= " + lastItem, Toast.LENGTH_SHORT).show();
                mMainViewModel.loadCompanies( lastItem + 1);
            }
        });


        Observer<List<Company>> companiesObserver = companies -> {
            Log.d(TAG, "companiesObserver.onChange: new count= " + (companies == null ? 0 : companies.size()));
            companiesAdapter.setData(companies);
        };

        mMainViewModel.getCompanies().observe(this, companiesObserver);
        mMainViewModel.loadCompanies(0);

        final SwipeController swipeController = new SwipeController(new SwipeControllerActions() {
            @Override
            public void onLeftClicked(int position) {
                Log.d(TAG, "onLeftClicked: delete " + position);
                companiesAdapter.removeItem(position);
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        DbManager.getInstance().deleteCompanyWithDelay(companiesAdapter.mCompanies.get(position));
                    }
                }, 400);




            }
        });

        companiesRv.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                super.onDraw(c, parent, state);
                swipeController.onDraw(c);
            }
        });
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(swipeController);
        itemTouchHelper.attachToRecyclerView(companiesRv);

        mMainViewModel.initializeImageFill();

        //set on item click listener:
        companiesAdapter.setOnItmeClickListener(id -> {
            Log.d(TAG, "onItemClick: " + id);
            startActivity(CompanyDetailsActivity.initActivity(MainActivity.this, id));

        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_company:
                Intent intent = new Intent(MainActivity.this, NewCompanyActivity.class);
                startActivity(intent);
                return true;
            case R.id.delete_all:
                DbManager.getInstance().deleteAll();
                return true;
//            case R.id.restore_base:

//                return true;
            case R.id.exit:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
