
package il.co.procyonapps.comexapp.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Companies {

    @SerializedName("companies")
    @Expose
    public List<Company> companies = null;

    @Override
    public String toString() {
        return "Companies{" +
                "companies=" + companies +
                '}';
    }
}
