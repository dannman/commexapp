package il.co.procyonapps.comexapp.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Hanan on 23/12/2017.
 */

public class ApiManager {
    private static ApiManager ourInstance;

    private GoogleSearchAPI mSearchAPI;
    private final Retrofit mRetrofit;

    public static ApiManager getInstance() {
        if (ourInstance == null) {
            ourInstance = new ApiManager();
        }
        return ourInstance;
    }

    private ApiManager() {
        mRetrofit = new Retrofit.Builder().baseUrl("https://www.googleapis.com").addConverterFactory(GsonConverterFactory.create()).build();
        mSearchAPI = mRetrofit.create(GoogleSearchAPI.class);
    }

    public GoogleSearchAPI services(){
        return mSearchAPI;
    }
}
