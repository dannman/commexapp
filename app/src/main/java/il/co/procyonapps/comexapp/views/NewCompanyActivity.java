package il.co.procyonapps.comexapp.views;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import il.co.procyonapps.comexapp.R;
import il.co.procyonapps.comexapp.data.DbManager;
import il.co.procyonapps.comexapp.data.MyExecutors;
import il.co.procyonapps.comexapp.network.ApiManager;
import il.co.procyonapps.comexapp.objects.Company;
import il.co.procyonapps.comexapp.objects.GSearchResults;
import il.co.procyonapps.comexapp.objects.Item;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Hanan on 24/12/2017.
 */

public class NewCompanyActivity extends AppCompatActivity implements OnMapReadyCallback {
    private final String TAG = this.getClass().getSimpleName();
    private static final int REQUEST_TAKE_PHOTO = 102;
    private EditText mName;
    private EditText mCity;
    private EditText mAddress;
    private Button mTakePicturBtn;
    private Button mAutoSearch;
    private SupportMapFragment mMapFragment;
    private ImageView mCompamyPicture;

    private Uri mPhotoURI;
    private String mImageUrl = null;

    private LatLng mCurrentLocation = null;
    private LatLng mInitialLocation;
    private Marker mMarker;
    private Button mUpdateAddress;
    private Button mSave;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_company);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        setTitle(getString(R.string.add_company_title));

        findViews();

        mTakePicturBtn.setOnClickListener(v -> takePicture());
        mAutoSearch.setOnClickListener(v -> searchByName());
        mUpdateAddress.setOnClickListener(v -> getAddressFromLocation(mCurrentLocation));
        mSave.setOnClickListener(v -> saveCompany());

        mInitialLocation = new LatLng(32.028921d, 34.7966447);

        mMapFragment.getMapAsync(this);
    }


    private void findViews() {
        mName = findViewById(R.id.et_name);
        mCity = findViewById(R.id.et_city);
        mAddress = findViewById(R.id.et_address);
        mTakePicturBtn = findViewById(R.id.take_picture);
        mAutoSearch = findViewById(R.id.auto_search);
        mMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_frag);
        mCompamyPicture = findViewById(R.id.iv_image);
        mUpdateAddress = findViewById(R.id.btn_update_address);
        mSave = findViewById(R.id.save);


    }

    private void searchByName() {
        String name = mName.getText().toString();
        if (!TextUtils.isEmpty(name)) {
            ApiManager.getInstance().services().getSearchResults(name + " logo").enqueue(new Callback<GSearchResults>() {
                @Override
                public void onResponse(Call<GSearchResults> call, Response<GSearchResults> response) {
                    String link = null;
                    final Item item = response.body().getItems().get(0);
                    if (!TextUtils.isEmpty(item.getLink())) {
                        link = item.getLink();
                    } else if (!TextUtils.isEmpty(item.getImage().getThumbnailLink())) {
                        link = item.getImage().getThumbnailLink();
                    }

                    if (link != null) {
                        Picasso.with(NewCompanyActivity.this).load(link).fit().into(mCompamyPicture);
                        mImageUrl = link;
                    }
                }

                @Override
                public void onFailure(Call<GSearchResults> call, Throwable t) {

                }
            });
        } else {
            Toast.makeText(this, "אנא הזן שם חברה קודם", Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK && mPhotoURI != null) {
            Picasso.with(this).load(mPhotoURI.toString()).into(mCompamyPicture);
            mImageUrl = mPhotoURI.toString();
        }
    }

    private void saveCompany() {
        if (valid()) {
            Company company = new Company(mName.getText().toString(), mCurrentLocation.latitude, mCurrentLocation.longitude, mCity.getText().toString(), mAddress.getText().toString(), mImageUrl);
            DbManager.getInstance().addCompany(company);
            finish();
        }
    }

    private boolean valid() {
        boolean valid = true;

        if (TextUtils.isEmpty(mName.getText().toString())) {
            mName.setError("שם חברה חסר");
            valid = false;
        }

        if (TextUtils.isEmpty(mCity.getText().toString())) {
            mCity.setError("עיר חסרה");
            valid = false;
        }

        if (TextUtils.isEmpty(mAddress.getText().toString())) {
            mAddress.setError("כתובת חסרה");
            valid = false;
        }

        if (mImageUrl == null) {
            Toast.makeText(this, "אנא הוסף תמונה", Toast.LENGTH_LONG).show();
            valid = false;
        }

        if (mCurrentLocation == null) {
            Toast.makeText(this, "אנא סמן מיקום במפה", Toast.LENGTH_LONG).show();
            valid = false;
        }

        return valid;
    }

    private void takePicture() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.e(TAG, "takePicture: error", ex);

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                mPhotoURI = FileProvider.getUriForFile(this,
                        "il.co.procyonapps.comexapp.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mPhotoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);

                Log.d(TAG, "takePicture: Uri= " + mPhotoURI.toString());
            }
        } else {
            Toast.makeText(this, "לא זוהתה מצלמה במכשיר", Toast.LENGTH_SHORT).show();
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "COMEX_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );


        return image;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mInitialLocation, 11f));
        mMarker = null;
        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                mCurrentLocation = latLng;
                if (mMarker != null) {
                    mMarker.remove();
                }
                mMarker = googleMap.addMarker(new MarkerOptions().position(latLng));

                googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

            }
        });
    }

    private void getAddressFromLocation(LatLng location) {
        WeakReference<Context> context = new WeakReference<Context>(this);
        MyExecutors.getInstance().getNetworkExecutor().execute(() -> {
            Geocoder geocoder = new Geocoder(context.get(), Locale.forLanguageTag("he"));

            try {
                Address address = geocoder.getFromLocation(location.latitude, location.longitude, 1).get(0);
                Log.d(TAG, "getAddressFromLocation: " + address);
                MyExecutors.getInstance().getUiExecutor().execute(() -> {
                    mCity.setText(address.getLocality());
                    mAddress.setText(address.getThoroughfare());
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        });
    }
}
